package se331.ltas.dao;

import se331.ltas.entity.Student;

import java.util.List;

public interface StudentDao {
    List<Student> getAllStudents();
}
