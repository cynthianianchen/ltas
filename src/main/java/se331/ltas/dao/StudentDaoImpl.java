package se331.ltas.dao;

import org.springframework.stereotype.Repository;
import se331.ltas.entity.Student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class StudentDaoImpl implements StudentDao {

    @Override
    public List<Student> getAllStudents() {
        return Arrays.asList(Student.builder().name("Test").build());
    }
}
