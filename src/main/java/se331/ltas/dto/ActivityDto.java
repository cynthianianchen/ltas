package se331.ltas.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ActivityDto {
    Integer activityId;
    String name;
    String location;
    String description;
    String periodOfRegistration;
    String date;
    TeacherDto host;
    String image;
}
