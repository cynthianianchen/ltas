package se331.ltas.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ActivityCommentDto {
    Integer commentId;
    ActivityDto activity;
    StudentDto postedByStudent;
    TeacherDto postedByTeacher;
    PersonDto postedByAdmin;

    String text;
    Date postedOn;
}
