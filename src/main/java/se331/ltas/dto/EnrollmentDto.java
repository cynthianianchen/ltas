package se331.ltas.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class EnrollmentDto {
    String enrollmentId;

    ActivityDto activity;
    StudentDto student;

    Boolean isApproved;
    Boolean isWaitingForApproval;
}
