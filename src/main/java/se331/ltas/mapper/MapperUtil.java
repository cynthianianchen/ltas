package se331.ltas.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import se331.ltas.dto.*;
import se331.ltas.entity.*;

import java.util.List;

@Mapper
public interface MapperUtil {
    MapperUtil INSTANCE = Mappers.getMapper( MapperUtil.class );

    @Mappings({})
    PersonDto getPersonDto(Person person);
    @Mappings({})
    List<PersonDto> getPersonDto(List<Person> persons);

    @Mappings({})
    Person getPersonFromDto(PersonDto person);

    @Mappings({})
    StudentDto getStudentDto(Student student);
    @Mappings({})
    List<StudentDto> getStudentDto(List<Student> students);

    @Mappings({})
    Student getStudentFromDto(StudentDto student);

    @Mappings({})
    TeacherDto getTeacherDto(Teacher teacher);
    @Mappings({})
    List<TeacherDto> getTeacherDto(List<Teacher> teachers);

    @Mappings({})
    ActivityDto getActivityDto(Activity activity);
    @Mappings({})
    List<ActivityDto> getActivityDto(List<Activity> activities);

    @Mappings({})
    Activity getActivityFromDto(ActivityDto activityDto);

    @Mappings({})
    EnrollmentDto getEnrollmentDto(Enrollment enrollment);

    @Mappings({})
    List<EnrollmentDto> getEnrollmentDto(List<Enrollment> enrollment);

    @Mappings({})
    ActivityCommentDto getActivityCommentsDto(ActivityComment activityComment);

    @Mappings({})
    List<ActivityCommentDto> getActivityCommentsDto(List<ActivityComment> activityComments);
}
