package se331.ltas.repository;

import org.springframework.data.repository.CrudRepository;
import se331.ltas.entity.Teacher;

import java.util.List;


public interface TeacherRepository extends CrudRepository<Teacher, String> {
    List<Teacher> findAll();
    Teacher findTeacherByUserCredentialsEmail(String email);
}
