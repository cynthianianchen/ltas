package se331.ltas.repository;

import org.springframework.data.repository.CrudRepository;
import se331.ltas.entity.Student;

import java.util.List;

public interface StudentRepository extends CrudRepository<Student, String> {
    List<Student> findAll();
    List<Student> findAllByIsWaitingForApprovalOrderBySignedUpOnDesc(Boolean isWaitingForApproval);
    Student findStudentByUserCredentialsEmail(String email);
}
