package se331.ltas.repository;

import org.springframework.data.repository.CrudRepository;
import se331.ltas.entity.Admin;
import se331.ltas.entity.Student;

import java.util.List;

public interface AdminRepository extends CrudRepository<Admin, String> {
    List<Admin> findAll();
    Admin findAdminByUserCredentialsEmail(String email);
}
