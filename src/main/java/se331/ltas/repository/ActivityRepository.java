package se331.ltas.repository;

import org.springframework.data.repository.CrudRepository;
import se331.ltas.entity.Activity;
import se331.ltas.entity.Teacher;

import java.util.List;

public interface ActivityRepository extends CrudRepository<Activity, Integer> {
    List<Activity> findAllByOrderByAddedOnDesc();
    List<Activity> findActivitiesByHost_UserCredentials_EmailOrderByAddedOnDesc(String email);
}
