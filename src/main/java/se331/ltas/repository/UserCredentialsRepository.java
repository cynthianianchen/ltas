package se331.ltas.repository;

import org.springframework.data.repository.CrudRepository;
import se331.ltas.entity.Student;
import se331.ltas.entity.UserCredentials;

public interface UserCredentialsRepository extends CrudRepository<UserCredentials, String> {
    UserCredentials findUserCredentialsByEmail(String email);
    UserCredentials findUserCredentialsByEmailAndUserType(String email, String userType);
}
