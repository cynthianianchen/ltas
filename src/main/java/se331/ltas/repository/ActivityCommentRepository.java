package se331.ltas.repository;

import org.springframework.data.repository.CrudRepository;
import se331.ltas.entity.ActivityComment;

import java.util.List;

public interface ActivityCommentRepository extends CrudRepository<ActivityComment, Integer> {
    List<ActivityComment> findAllByActivityActivityIdOrderByPostedOnDesc(Integer activityId);
}
