package se331.ltas.repository;

import org.springframework.data.repository.CrudRepository;
import se331.ltas.entity.Enrollment;

import java.util.List;

public interface EnrollmentRepository extends CrudRepository<Enrollment, String> {
    Enrollment findByStudentEmailAndActivityActivityId(String studentEmail, Integer activityId);
    List<Enrollment> findAllByStudentEmail(String studentEmail);
    List<Enrollment> findAllByActivityActivityId(Integer activityId);
}
