package se331.ltas.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import se331.ltas.dto.StudentDto;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class StudentRegistrationRequest {
    StudentDto student;
    String password;
}
