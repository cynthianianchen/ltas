package se331.ltas.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import se331.ltas.dto.PersonDto;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class PersonProfileUpdateRequest {
    PersonDto person;
    String password;
}
