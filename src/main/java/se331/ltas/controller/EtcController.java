package se331.ltas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import se331.ltas.service.EtcService;

@RestController
public class EtcController {
    @Autowired
    private EtcService etcService;

    @GetMapping("/public/etc/reset")
    public ResponseEntity<?> reset() {
        etcService.resetAll();

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
