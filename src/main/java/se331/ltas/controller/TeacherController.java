package se331.ltas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import se331.ltas.dto.StudentDto;
import se331.ltas.dto.TeacherDto;
import se331.ltas.mapper.MapperUtil;
import se331.ltas.service.TeacherService;

import java.util.List;

@RestController
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    @GetMapping("/admin/teachers")
    public ResponseEntity<List<TeacherDto>> getAll() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getTeacherDto(teacherService.getAll()));
    }
}
