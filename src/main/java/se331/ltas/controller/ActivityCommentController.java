package se331.ltas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import se331.ltas.dto.ActivityCommentDto;
import se331.ltas.entity.*;
import se331.ltas.enums.UserType;
import se331.ltas.mapper.MapperUtil;
import se331.ltas.service.ActivityCommentService;
import se331.ltas.service.PersonService;

import java.util.Date;
import java.util.List;

@RestController
public class ActivityCommentController {
    @Autowired
    ActivityCommentService activityCommentService;

    @Autowired
    PersonService personService;

    @PostMapping("/studentAndTeacher/activityComments")
    public ResponseEntity<ActivityCommentDto> addComment(@RequestBody ActivityComment activityComment) {
        String postedByEmail = SecurityContextHolder.getContext().getAuthentication().getName();

        activityComment.setPostedByStudent((Student)personService.getPerson(postedByEmail, UserType.STUDENT.toString()));
        activityComment.setPostedByTeacher((Teacher)personService.getPerson(postedByEmail, UserType.TEACHER.toString()));
        activityComment.setPostedByAdmin((Admin)personService.getPerson(postedByEmail, UserType.ADMIN.toString()));

        activityComment.setPostedOn(new Date());

        activityCommentService.addComment(activityComment);

        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityCommentsDto(activityComment));
    }

    @DeleteMapping("/admin/activityComments/{activityCommentId}")
    public ResponseEntity<?> deleteComment(@PathVariable("activityCommentId") Integer activityCommentId) {
        activityCommentService.deleteComment(activityCommentId);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/public/activityComments/{activityId}")
    public ResponseEntity<List<ActivityCommentDto>> getAllComments(@PathVariable("activityId") Integer activityId) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityCommentsDto(activityCommentService.getAllComments(activityId)));
    }
}
