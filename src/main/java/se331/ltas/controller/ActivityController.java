package se331.ltas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import se331.ltas.dto.ActivityDto;
import se331.ltas.entity.Activity;
import se331.ltas.mapper.MapperUtil;
import se331.ltas.service.ActivityService;

import java.util.List;

@RestController
public class ActivityController {
    @Autowired
    ActivityService activityService;

    @PostMapping("/admin/activities/new")
    public ResponseEntity<?> newActivity(@RequestBody ActivityDto newActivity) throws Exception {
        activityService.newActivity(MapperUtil.INSTANCE.getActivityFromDto(newActivity));

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/public/activities")
    public ResponseEntity<List<ActivityDto>> getAll() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.getAll()));
    }

    @GetMapping("/teacher/activities/getAllOwn")
    public ResponseEntity<List<ActivityDto>> getAllOwn() {
        String teacherEmail = SecurityContextHolder.getContext().getAuthentication().getName();

        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.getAllByTeacher(teacherEmail)));
    }

    @GetMapping("/public/activities/getActivity/{activityId}")
    public ResponseEntity<ActivityDto> getActivity(@PathVariable("activityId") Integer activityId) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.getActivity(activityId)));
    }
}
