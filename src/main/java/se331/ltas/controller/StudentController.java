package se331.ltas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se331.ltas.dto.StudentDto;
import se331.ltas.mapper.MapperUtil;
import se331.ltas.request.StudentRegistrationRequest;
import se331.ltas.service.StudentService;

import java.util.List;

@RestController
public class StudentController {
    @Autowired
    StudentService studentService;

    @PostMapping("/public/students/register")
    public ResponseEntity<?> register(@RequestBody StudentRegistrationRequest registrationRequest) throws Exception {
        studentService.register(MapperUtil.INSTANCE.getStudentFromDto(registrationRequest.getStudent()), registrationRequest.getPassword());

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/admin/students/loadWaitingForApproval")
    public ResponseEntity<List<StudentDto>> getAllWaitingForApproval() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.getAllWaitingApprovalStudents()));
    }

    @PostMapping("/admin/students/approve")
    public ResponseEntity<?> approveStudent(@RequestBody String email) throws Exception {
        studentService.approve(email);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/admin/students/reject")
    public ResponseEntity<?> rejectStudent(@RequestBody String email) throws Exception {
        studentService.reject(email);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
