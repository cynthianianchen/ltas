package se331.ltas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import se331.ltas.mapper.MapperUtil;
import se331.ltas.request.PersonProfileUpdateRequest;
import se331.ltas.service.PersonService;

@RestController
public class PersonController {
    @Autowired
    PersonService personService;

    @PostMapping("/public/persons/update")
    public ResponseEntity<String> update(@RequestBody PersonProfileUpdateRequest updateRequest) throws Exception {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        personService.updatePerson(MapperUtil.INSTANCE.getPersonFromDto(updateRequest.getPerson()), updateRequest.getPassword());

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
