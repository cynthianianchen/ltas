package se331.ltas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import se331.ltas.dto.EnrollmentDto;
import se331.ltas.entity.Enrollment;
import se331.ltas.mapper.MapperUtil;
import se331.ltas.service.EnrollmentService;

import java.util.List;

@RestController
public class EnrollmentController {
    @Autowired
    EnrollmentService enrollmentService;

    @GetMapping("/student/enrollments/get/{activityId}")
    public ResponseEntity<?> getStudentEnrollment(@PathVariable("activityId") Integer activityId) {
        String studentEmail = SecurityContextHolder.getContext().getAuthentication().getName();

        return ResponseEntity.ok(MapperUtil.INSTANCE.getEnrollmentDto(this.enrollmentService.getStudentEnrollment(studentEmail, activityId)));
    }

    @GetMapping("/student/enrollments/enroll/{activityId}")
    public ResponseEntity<?> enroll(@PathVariable("activityId") Integer activityId) {
        String studentEmail = SecurityContextHolder.getContext().getAuthentication().getName();

        return ResponseEntity.ok(MapperUtil.INSTANCE.getEnrollmentDto(this.enrollmentService.enroll(studentEmail, activityId)));
    }

    @GetMapping("/student/enrollments")
    public ResponseEntity<?> getOwnEnrollments() {
        String studentEmail = SecurityContextHolder.getContext().getAuthentication().getName();

        return ResponseEntity.ok(MapperUtil.INSTANCE.getEnrollmentDto(
                enrollmentService.getOwnEnrollments(studentEmail)));
    }

    @GetMapping("/teacher/enrollments/activity/{activityId}")
    public ResponseEntity<?> getActivityEnrollments(@PathVariable("activityId") Integer activityId) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getEnrollmentDto(
                enrollmentService.getActivityEnrollments(activityId)));
    }

    @GetMapping("/teacher/enrollments/approve/{enrollmentId}")
    public ResponseEntity<?> approveEnrollment(@PathVariable("enrollmentId") String enrollmentId) {
        enrollmentService.approveEnrollment(enrollmentId);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/teacher/enrollments/reject/{enrollmentId}")
    public ResponseEntity<?> rejectEnrollment(@PathVariable("enrollmentId") String enrollmentId) {
        enrollmentService.rejectEnrollment(enrollmentId);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
