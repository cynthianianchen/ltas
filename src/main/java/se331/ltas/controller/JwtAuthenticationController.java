package se331.ltas.controller;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import se331.ltas.config.JwtTokenUtil;
import se331.ltas.entity.JwtUserDetails;
import se331.ltas.entity.Person;
import se331.ltas.entity.UserCredentials;
import se331.ltas.mapper.MapperUtil;
import se331.ltas.request.JwtRequest;
import se331.ltas.response.JwtResponse;
import se331.ltas.service.PersonService;
import se331.ltas.service.UserCredentialsService;


@RestController
@CrossOrigin
public class JwtAuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserCredentialsService userCredentialsService;

    @Autowired
    private PersonService personService;

    @PostMapping("/public/auth")
    public ResponseEntity<JwtResponse> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());

        UserCredentials userCredentials = userCredentialsService.loadUserCredentials(authenticationRequest.getEmail());

        Person person = personService.getPerson(userCredentials.getEmail(), userCredentials.getUserType());

        final String token = jwtTokenUtil.generateToken(new JwtUserDetails(userCredentials));
        return ResponseEntity.ok(new JwtResponse(token, userCredentials.getUserType(),
                MapperUtil.INSTANCE.getPersonDto(person)));
    }

    @GetMapping("/public/authorize")
    public ResponseEntity<JwtResponse> checkAuth() throws Exception {
        ResponseEntity<JwtResponse> result;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null) {
            String email = SecurityContextHolder.getContext().getAuthentication().getName();

            if (!email.equals("anonymousUser")) {
                UserCredentials userCredentials = userCredentialsService.loadUserCredentials(email);

                if (userCredentials != null) {
                    Person person = personService.getPerson(userCredentials.getEmail(), userCredentials.getUserType());

                    if (person != null) {
                        result = ResponseEntity.ok(new JwtResponse(null, userCredentials.getUserType(),
                                MapperUtil.INSTANCE.getPersonDto(person)));
                    } else {
                        throw new Exception("Person is not found");
                    }
                } else {
                    throw new Exception("User credentials are not found");
                }
            } else {
                throw new Exception("Not authenticated");
            }
        } else {
            throw new Exception("Not authenticated");
        }

        return result;
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("User disabled");
        } catch (BadCredentialsException e) {
            throw new Exception("Invalid credentials");
        }
    }
}