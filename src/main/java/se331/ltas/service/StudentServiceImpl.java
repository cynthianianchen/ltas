package se331.ltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import se331.ltas.dao.StudentDao;
import se331.ltas.entity.Student;
import se331.ltas.entity.UserCredentials;
import se331.ltas.enums.UserType;
import se331.ltas.repository.StudentRepository;
import se331.ltas.repository.UserCredentialsRepository;
import se331.ltas.request.StudentRegistrationRequest;

import java.util.Date;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private UserCredentialsRepository userCredentialsRepository;

    StudentServiceImpl() {

    }

    @Override
    public List<Student> getAllWaitingApprovalStudents() {
        return studentRepository.findAllByIsWaitingForApprovalOrderBySignedUpOnDesc(true);
    }

    @Override
    public Student getStudent(String email) {
        return studentRepository.findStudentByUserCredentialsEmail(email);
    }

    @Override
    public void register(Student student, String password) throws Exception {
        PasswordEncoder encoder = new BCryptPasswordEncoder();

        student.setIsWaitingForApproval(true);
        student.setSignedUpOn(new Date());

        if (userCredentialsRepository.existsById(student.getEmail())) {
            throw new Exception("User with this email already exists");
        }

        UserCredentials userCredentials = UserCredentials.builder().
                email(student.getEmail()).
                password(encoder.encode(password)).
                userType(UserType.STUDENT.toString()).isActivated(false).build();

        student.setUserCredentials(userCredentials);

        studentRepository.save(student);
    }

    @Override
    public void approve(String email) throws Exception {
        UserCredentials userCredentials = userCredentialsRepository.findUserCredentialsByEmailAndUserType(email, UserType.STUDENT.toString());

        if (userCredentials != null) {
            userCredentials.setIsActivated(true);

            userCredentialsRepository.save(userCredentials);

            Student student = studentRepository.findStudentByUserCredentialsEmail(email);

            if (student != null) {
                student.setIsWaitingForApproval(false);
                studentRepository.save(student);
            }
        } else {
            throw new Exception("User credentials are not found or don't belong to student");
        }
    }

    @Override
    public void reject(String email) throws Exception {
        UserCredentials userCredentials = userCredentialsRepository.findUserCredentialsByEmailAndUserType(email, UserType.STUDENT.toString());

        if (userCredentials != null) {
            userCredentials.setIsActivated(false);

            userCredentialsRepository.save(userCredentials);

            Student student = studentRepository.findStudentByUserCredentialsEmail(email);

            if (student != null) {
                student.setIsWaitingForApproval(false);
                studentRepository.save(student);
            }
        } else {
            throw new Exception("User credentials are not found or don't belong to student");
        }
    }
}