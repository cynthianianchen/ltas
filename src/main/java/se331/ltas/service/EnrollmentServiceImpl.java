package se331.ltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.ltas.entity.Activity;
import se331.ltas.entity.Enrollment;
import se331.ltas.entity.Student;
import se331.ltas.repository.ActivityRepository;
import se331.ltas.repository.EnrollmentRepository;
import se331.ltas.repository.StudentRepository;

import java.util.List;

@Service
public class EnrollmentServiceImpl implements EnrollmentService {
    @Autowired
    private EnrollmentRepository enrollmentRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private ActivityRepository activityRepository;

    @Override
    public Enrollment getStudentEnrollment(String studentEmail, Integer activityId) {
        return this.enrollmentRepository.findByStudentEmailAndActivityActivityId(studentEmail, activityId);
    }

    @Override
    public Enrollment enroll(String studentEmail, Integer activityId) {
        Student student = studentRepository.findById(studentEmail).get();
        Activity activity = activityRepository.findById(activityId).get();

        String enrollmentId = studentEmail + ":" + activityId;

        Enrollment enrollment = Enrollment.builder().
                enrollmentId(enrollmentId).student(student).activity(activity).
                isWaitingForApproval(true).isApproved(false).build();

        enrollmentRepository.save(enrollment);

        return enrollment;
    }

    @Override
    public List<Enrollment> getOwnEnrollments(String studentEmail) {
        return enrollmentRepository.findAllByStudentEmail(studentEmail);
    }

    @Override
    public List<Enrollment> getActivityEnrollments(Integer activityId) {
        return enrollmentRepository.findAllByActivityActivityId(activityId);
    }

    @Override
    public void approveEnrollment(String enrollmentId) {
        Enrollment enrollment = enrollmentRepository.findById(enrollmentId).get();

        enrollment.setIsApproved(true);
        enrollment.setIsWaitingForApproval(false);

        enrollmentRepository.save(enrollment);
    }

    @Override
    public void rejectEnrollment(String enrollmentId) {
        Enrollment enrollment = enrollmentRepository.findById(enrollmentId).get();

        enrollment.setIsApproved(false);
        enrollment.setIsWaitingForApproval(false);

        enrollmentRepository.save(enrollment);
    }
}
