package se331.ltas.service;

import se331.ltas.entity.Teacher;

import java.util.List;

public interface TeacherService {
    List<Teacher> getAll();
}
