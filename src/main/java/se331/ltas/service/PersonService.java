package se331.ltas.service;

import se331.ltas.entity.Person;
import se331.ltas.request.PersonProfileUpdateRequest;

public interface PersonService {
    Person getPerson(String email, String userType);
    void updatePerson(Person person, String password) throws Exception;
}
