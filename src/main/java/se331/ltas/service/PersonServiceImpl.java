package se331.ltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import se331.ltas.entity.*;
import se331.ltas.enums.UserType;
import se331.ltas.repository.AdminRepository;
import se331.ltas.repository.StudentRepository;
import se331.ltas.repository.TeacherRepository;
import se331.ltas.repository.UserCredentialsRepository;
import se331.ltas.request.PersonProfileUpdateRequest;

@Service
public class PersonServiceImpl implements PersonService {
    @Autowired
    UserCredentialsRepository userCredentialsRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    AdminRepository adminRepository;

    @Override
    public Person getPerson(String email, String userType) {
        Person person = null;

        if (userType.equals(UserType.STUDENT.toString())) {
            person = studentRepository.findStudentByUserCredentialsEmail(email);
        } else if (userType.equals(UserType.TEACHER.toString())) {
            person = teacherRepository.findTeacherByUserCredentialsEmail(email);
        } else if (userType.equals(UserType.ADMIN.toString())) {
            person = adminRepository.findAdminByUserCredentialsEmail(email);
        }

        return person;
    }

    @Override
    public void updatePerson(Person person, String password) throws Exception {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        UserCredentials userCredentials = userCredentialsRepository.findUserCredentialsByEmail(person.getEmail());

        if (userCredentials != null) {
            if (userCredentials.getUserType().equals(UserType.STUDENT.toString())) {
                Student student = studentRepository.findStudentByUserCredentialsEmail(person.getEmail());

                student.applyUpdate(person);

                studentRepository.save(student);
            } else if (userCredentials.getUserType().equals(UserType.TEACHER.toString())) {
                Teacher teacher = teacherRepository.findTeacherByUserCredentialsEmail(person.getEmail());

                teacher.applyUpdate(person);

                teacherRepository.save(teacher);
            } else if (userCredentials.getUserType().equals(UserType.ADMIN.toString())) {
                Admin admin = adminRepository.findAdminByUserCredentialsEmail(person.getEmail());

                admin.applyUpdate(person);

                adminRepository.save(admin);
            } else {
                throw new Exception("Person profile is not found");
            }

            userCredentials.setPassword(encoder.encode(password));

            userCredentialsRepository.save(userCredentials);

        } else {
            throw new Exception("Person is not found");
        }
    }
}
