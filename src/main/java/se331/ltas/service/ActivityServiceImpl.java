package se331.ltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.ltas.entity.Activity;
import se331.ltas.entity.Teacher;
import se331.ltas.repository.ActivityRepository;
import se331.ltas.repository.TeacherRepository;

import java.util.Date;
import java.util.List;

@Service
public class ActivityServiceImpl implements ActivityService {
    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    ActivityRepository activityRepository;

    public void newActivity(Activity newActivity) throws Exception {
        newActivity.setAddedOn(new Date());

        activityRepository.save(newActivity);
    }

    public List<Activity> getAllByTeacher(String teacherEmail) {
        return activityRepository.findActivitiesByHost_UserCredentials_EmailOrderByAddedOnDesc(teacherEmail);
    }

    public Activity getActivity(Integer activityId) {
        return activityRepository.findById(activityId).get();
    }

    public List<Activity> getAll() {
        return activityRepository.findAllByOrderByAddedOnDesc();
    }
}
