package se331.ltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.ltas.entity.UserCredentials;
import se331.ltas.repository.UserCredentialsRepository;

@Service
public class UserCredentialsServiceImpl implements UserCredentialsService {
    @Autowired
    UserCredentialsRepository userCredentialsRepository;

    @Override
    public UserCredentials loadUserCredentials(String email) {
        return userCredentialsRepository.findUserCredentialsByEmail(email);
    }
}
