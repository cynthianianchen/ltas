package se331.ltas.service;

import org.springframework.stereotype.Service;
import se331.ltas.entity.UserCredentials;

public interface UserCredentialsService {
    UserCredentials loadUserCredentials(String email);
}
