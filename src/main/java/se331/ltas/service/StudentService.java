package se331.ltas.service;

import se331.ltas.entity.Student;

import java.util.List;

public interface StudentService {
    List<Student> getAllWaitingApprovalStudents();
    Student getStudent(String email);
    void register(Student student, String password) throws Exception;
    void approve(String email) throws Exception;
    void reject(String email) throws Exception;
}
