package se331.ltas.service;

import se331.ltas.entity.ActivityComment;

import java.util.List;

public interface ActivityCommentService {
    void addComment(ActivityComment activityComment);
    List<ActivityComment> getAllComments(Integer activityId);
    void deleteComment(Integer activityCommentId);
}
