package se331.ltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.ltas.entity.ActivityComment;
import se331.ltas.repository.ActivityCommentRepository;

import java.util.List;

@Service
public class ActivityCommentServiceImpl implements ActivityCommentService {
    @Autowired
    private ActivityCommentRepository activityCommentRepository;

    @Override
    public void addComment(ActivityComment activityComment) {
        this.activityCommentRepository.save(activityComment);
    }

    @Override
    public List<ActivityComment> getAllComments(Integer activityId) {
        return this.activityCommentRepository.findAllByActivityActivityIdOrderByPostedOnDesc(activityId);
    }

    @Override
    public void deleteComment(Integer activityCommentId) {
        this.activityCommentRepository.deleteById(activityCommentId);
    }
}
