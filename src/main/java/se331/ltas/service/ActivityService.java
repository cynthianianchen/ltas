package se331.ltas.service;

import se331.ltas.entity.Activity;

import java.util.List;

public interface ActivityService {
    void newActivity(Activity newActivity) throws Exception;
    List<Activity> getAllByTeacher(String teacherEmail);
    Activity getActivity(Integer activityId);
    List<Activity> getAll();
}
