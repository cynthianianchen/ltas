package se331.ltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import se331.ltas.entity.JwtUserDetails;
import se331.ltas.entity.UserCredentials;
import se331.ltas.repository.UserCredentialsRepository;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserCredentialsRepository userCredentialsRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserCredentials userCredentials = userCredentialsRepository.findUserCredentialsByEmail(username);

        if (userCredentials == null) {
            throw new UsernameNotFoundException("Username is not found");
        }

        return new JwtUserDetails(userCredentials);
    }
}
