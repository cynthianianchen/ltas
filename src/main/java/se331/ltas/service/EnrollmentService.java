package se331.ltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import se331.ltas.entity.Enrollment;
import se331.ltas.repository.ActivityRepository;
import se331.ltas.repository.StudentRepository;

import java.util.List;

public interface EnrollmentService {
    Enrollment getStudentEnrollment(String studentEmail, Integer activityId);
    Enrollment enroll(String studentEmail, Integer activityId);
    List<Enrollment> getOwnEnrollments(String studentEmail);
    List<Enrollment> getActivityEnrollments(Integer activityId);
    void approveEnrollment(String enrollmentId);
    void rejectEnrollment(String enrollmentId);
}
