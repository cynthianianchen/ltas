package se331.ltas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import se331.ltas.entity.*;
import se331.ltas.repository.*;

import java.util.Date;

@Service
public class EtcServiceImpl implements EtcService {
    @Autowired
    private UserCredentialsRepository userCredentialsRepository;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private ActivityCommentRepository activityCommentRepository;

    @Autowired
    private EnrollmentRepository enrollmentRepository;

    private void addAdmin() {
        PasswordEncoder encoder = new BCryptPasswordEncoder();

        UserCredentials userCredentials = UserCredentials.builder().
                email("admin@admin.com").
                password(encoder.encode("admin")).
                userType("ADMIN").
                isActivated(true).
                build();

        Admin admin = Admin.builder().
                userCredentials(userCredentials).
                name("Admin").
                lastName("Admin").
                signedUpOn(new Date()).
                image("https://www.googleapis.com/download/storage/v1/b/imageupload-9d058.appspot.com/o/2019-12-23%20041645753-images.jpeg?generation=1577049407192931&alt=media").
                build();

        this.adminRepository.save(admin);
    }

    private void addTeacher(int teacherIndex) {
        PasswordEncoder encoder = new BCryptPasswordEncoder();

        UserCredentials userCredentials = UserCredentials.builder().
                email("teacher" + teacherIndex  + "@ltas.com").
                password(encoder.encode("teacher" + teacherIndex)).
                userType("TEACHER").
                isActivated(true).
                build();

        Teacher teacher = Teacher.builder().
                userCredentials(userCredentials).
                name("TeacherName" + teacherIndex).
                lastName("Teacher").
                image("https://www.googleapis.com/download/storage/v1/b/imageupload-9d058.appspot.com/o/2019-12-23%20041645753-images.jpeg?generation=1577049407192931&alt=media").
                signedUpOn(new Date()).
                build();

        this.teacherRepository.save(teacher);

        Activity activity = Activity.builder().
                name(teacher.getName() + "'s activity").
                description(teacher.getName() + "'s activity").
                location("camt").periodOfRegistration("10").date("2019-12-25T17:00:00.000Z").
                host(teacher).
                addedOn(new Date()).build();

        this.activityRepository.save(activity);
    }

    private void addStudent(int studentIndex) {
        PasswordEncoder encoder = new BCryptPasswordEncoder();

        UserCredentials userCredentials = UserCredentials.builder().
                email("student" + studentIndex  + "@ltas.com").
                password(encoder.encode("student" + studentIndex)).
                userType("STUDENT").
                isActivated(true).
                build();

        Student student = Student.builder().
                userCredentials(userCredentials).
                studentId(100000000l + studentIndex).
                name("StudentName" + studentIndex).
                lastName("Student").
                signedUpOn(new Date()).
                image("https://www.googleapis.com/download/storage/v1/b/imageupload-9d058.appspot.com/o/2019-12-23%20041645753-images.jpeg?generation=1577049407192931&alt=media").
                isWaitingForApproval(false).
                build();

        this.studentRepository.save(student);
    }

    @Override
    public void resetAll() {
        this.enrollmentRepository.deleteAll();;
        this.activityCommentRepository.deleteAll();
        this.activityRepository.deleteAll();
        this.adminRepository.deleteAll();
        this.teacherRepository.deleteAll();
        this.studentRepository.deleteAll();
        this.userCredentialsRepository.deleteAll();

        this.addAdmin();

        for (int i = 1; i <= 10; i++) {
            this.addTeacher(i);
        }

        for (int i = 1; i <= 50; i++) {
            this.addStudent(i);
        }

//        this.teacherRepository.save(
//                Teacher.builder().
//                        email("teacher1@test.com").
//                        name("TeacherName1").
//                        lastName("TeacherLastName1").
//                        signedUpOn(new Date()).
//                        build()
//        );
//
//        this.teacherRepository.save(
//                Teacher.builder().
//                        email("teacher2@test.com").
//                        name("TeacherName2").
//                        lastName("TeacherLastName2").
//                        signedUpOn(new Date()).
//                        build()
//        );
//
//        this.teacherRepository.save(
//                Teacher.builder().
//                        email("teacher3@test.com").
//                        name("TeacherName3").
//                        lastName("TeacherLastName3").
//                        signedUpOn(new Date()).
//                        build()
//        );
//
//        this.studentRepository.save(
//                Student.builder().
//                        email("student1@test.com").
//                        name("StudentName1").
//                        lastName("StudentLastName1").
//                        signedUpOn(new Date()).
//                        isWaitingForApproval(true).build()
//        );
//
//        this.studentRepository.save(
//                Student.builder().
//                        email("student2@test.com").
//                        name("StudentName2").
//                        lastName("StudentLastName2").
//                        signedUpOn(new Date()).
//                        isWaitingForApproval(true).build()
//        );
//
//        this.studentRepository.save(
//                Student.builder().
//                        email("student3@test.com").
//                        name("StudentName3").
//                        lastName("StudentLastName3").
//                        signedUpOn(new Date()).
//                        isWaitingForApproval(true).build()
//        );

    }
}
