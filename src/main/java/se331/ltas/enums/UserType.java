package se331.ltas.enums;

public enum UserType {
    STUDENT,
    TEACHER,
    ADMIN
}
