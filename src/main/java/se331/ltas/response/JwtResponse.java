package se331.ltas.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import se331.ltas.dto.PersonDto;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class JwtResponse {
    String token;
    String userType;
    PersonDto person;
}
