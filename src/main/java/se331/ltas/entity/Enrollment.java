package se331.ltas.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Enrollment {
    @Id
    String enrollmentId;

    @ManyToOne
    Activity activity;
    @ManyToOne
    Student student;

    Boolean isApproved;
    Boolean isWaitingForApproval;
}
