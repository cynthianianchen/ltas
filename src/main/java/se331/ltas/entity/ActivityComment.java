package se331.ltas.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ActivityComment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer commentId;
    @ManyToOne
    Activity activity;

    @ManyToOne
    Student postedByStudent;

    @ManyToOne
    Teacher postedByTeacher;

    @ManyToOne
    Admin postedByAdmin;

    String text;
    Date postedOn;
}
