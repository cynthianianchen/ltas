package se331.ltas.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Student extends Person {
    Long studentId;
    String image;
    @OneToMany(mappedBy = "student")
    List<Enrollment> enrollments;

    Boolean isWaitingForApproval;
}
