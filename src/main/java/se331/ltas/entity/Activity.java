package se331.ltas.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer activityId;
    String name;
    String location;
    String description;
    String periodOfRegistration;
    String date;
    String image;
    @ManyToOne
    Teacher host;
    @OneToMany(mappedBy = "activity")
    List<Enrollment> enrollments;
    Date addedOn;
}
