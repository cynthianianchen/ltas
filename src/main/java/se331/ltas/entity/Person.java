package se331.ltas.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SuperBuilder
public class Person {
    @Id
    String email;

    String name;
    String lastName;
    String image;

    @OneToOne(cascade = CascadeType.ALL)
    @MapsId
    UserCredentials userCredentials;

    Date signedUpOn;

    public void applyUpdate(Person personUpdate) {
        this.name = personUpdate.getName();
        this.lastName = personUpdate.getLastName();
    }
}
