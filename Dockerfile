FROM openjdk:8-jdk-alpine
EXPOSE 8239

COPY target/ltas-backend.jar app.jar

ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", \
	"-Dspring.profiles.active=dev-server",\
	"-jar", "/app.jar"]